# Call Recorder

Android friendly!

Call Recorder with custom recording folder.

MOST PHONES DOES NOT SUPPORT CALL RECORDING. Blame Google or your phone manufacturer not me!

If you have any audio issues (no opponent voice, your voice muted or any else). Try Encoder / ogg + all sources, then Encoder / aac / Media Recorder + all sources, if you still experince audio issues - then your phone do not support call recording.

If it fails with high quality sound recording (voice line) this app will switch back to the MIC recording, no ads, open-source, GPLv3.

Google knows about issue but do nothing, can read more here:

https://code.google.com/p/android/issues/detail?id=206613#c13

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Call Recorder' to your language  please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)

# Survey Results

User filled survey results from app About menu.

  * https://docs.google.com/spreadsheets/d/1C6HuLIk3k4SmAYEuKOnQynGEPM39rjlu90oX7bYbqLk/edit?usp=sharing

# Contributors

  * japaness translation thanks to @naofumi
  * greek tralsation thanks to @cryoranger
  * german tralstaion thanks to @aenaehcwh
